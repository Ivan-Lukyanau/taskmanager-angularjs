const list = [
    {
        id: 1,
        name: 'First list',
    },
    {
        id: 2,
        name: 'Second list',
    },
];

module.exports = function(app){

    app.get('/', function(req, res) {
         // load the single view file (angular will handle the page changes on the front-end)
        res.sendfile('./public/index.html');
    });

    app.get('/lists', function(req, res, next){
        res.send(list);        
    });
}