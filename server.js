const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const http = require('http');

const router = require('./router');

const app = express();
app.use(morgan('dev'));
app.use(bodyParser.json({ type: '*/*'}));

app.use(express.static(__dirname + '/public'));

router(app);

const port = process.env.PORT || 3091;
const server = http.createServer(app);
server.listen(port);
console.log('Server is running on port : ', port);